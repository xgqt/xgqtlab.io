#!/usr/bin/env racket

;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

#lang racket/base

(require (only-in racket/cmdline command-line))
(require (only-in xml display-xml/content xexpr->xml))
(require "pages.rkt")

(provide main)

(define (main)
  (command-line
   #:args (output-directory)
   (for ([(f x) (in-hash html-content-assoc)])
     (with-output-to-file (build-path output-directory f)
       (lambda ()
         (displayln "<!DOCTYPE html>")
         (display-xml/content (xexpr->xml x))
         (newline))
       #:exists 'replace))))

(module+ main
  (main))

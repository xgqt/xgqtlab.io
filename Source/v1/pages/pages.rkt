#!/usr/bin/env racket

;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

#lang racket/base

(provide (all-defined-out))

(define dd-list-find
  #(#("mailto:xgqt@xgqt.org"
      "fa fa-envelope"
      "E-Mail")
    #("https://www.linkedin.com/in/xgqt/"
      "fa fa-linkedin-square"
      "Linkedin")
    #("https://emacs.ch/@xgqt"
      "fa fa-bullhorn"
      "Mastodon")
    #("https://repology.org/projects/?search=&maintainer=xgqt%40gentoo.org"
      "fa fa-plus-square"
      "Repology")
    #("https://wiki.gentoo.org/wiki/User:Xgqt"
      "fl-gentoo"
      "Gentoo Wiki")))

(define dd-list-development
  #(#("./projects.html"
      "fa fa-git"
      "Projects")
    #("https://gitlab.com/xgqt/"
      "fa fa-gitlab"
      "GitLab")
    #("https://github.com/xgqt/"
      "fa fa-github"
      "GitHub")
    #("https://www.openhub.net/accounts/xgqt"
      "fa fa-h-square"
      "OpenHub")
    #("https://gitweb.gentoo.org/repo/gentoo.git/log/?qt=committer&q=xgqt@gentoo.org"
      "fa fa-cube"
      "Gentoo")))

(define dd-list-more
  #(#("./gpg.html"
      "fa fa-key"
      "GPG Key")
    #("https://xgqt.gitlab.io/blog/"
      "fa fa-pencil"
      "Blog")
    #("https://xgqt.gitlab.io/startpage/"
      "fa fa-sitemap"
      "Startpage")
    #("https://xgqt.gitlab.io/spywarewatchdog/"
      "fa fa-eye"
      "Spyware Watchdog")
    #("./donate.html"
      "fa fa-money"
      "Donate")))

(define xexpr-sidebar
  `(div
    ((id "left"))
    ,@(for/list ([dt-section
                  #(#("fa fa-search" "Find me on:")
                    #("fa fa-code-fork" "Development:")
                    #("fa fa-info-circle" "More:"))]
                 [dd-list
                  (list dd-list-find dd-list-development dd-list-more)])
        `(dl
          (dt
           (i ((class ,(vector-ref dt-section 0)))) ,(vector-ref dt-section 1))
          ,@(for/list ([dd dd-list])
              `(dd (a ((href ,(vector-ref dd 0)))
                      (i ((class ,(vector-ref dd 1))))
                      ,(vector-ref dd 2))))))))

(define (xexpr-main title xexpr-top xexpr-right)
  `(html
    (head
     (meta ((charset "utf-8")))
     (meta ((name "generator")
            (content "GitLab Pages")))
     (meta ((name "description")
            (content "XGQT's Site")))
     (meta ((name "keywords")
            (content "xgqt, xgqt.gitlab.io, user site, gitlab, gitlab.io")))
     (meta ((name "author")
            (content "XGQT")))

     (title ,title)

     (link ((rel "icon")
            (href "./assets/icons/favicon.ico")))
     (link ((rel "stylesheet")
            (href "./assets/styles/main.css")))

     (link ((rel "stylesheet")
            (href "./assets/fonts/awesome/css/font-awesome.css")))
     (link ((rel "stylesheet")
            (href "./assets/fonts/logos/assets/font-logos.css"))))
    (body
     (div ((id "box")) ,xexpr-sidebar ,xexpr-top ,xexpr-right))))

(define xexpr-index-top
  '(div ((id "top")) (h1 "XGQT's Site")))

(define xexpr-index-right
  '(div
    ((id "right"))
    (h2 "Hello!")
    (p "My name is Maciej Barć, also known as XGQT")
    (p "I'm a software engineer interested in programming language development"
       "and DSLs as well as software packaging."
       "I actively take part in Open Source projects.")
    (p
     "To count some of my favorite topics:"
     (a ((href "https://www.gnu.org/")) "GNU/")
     (a ((href "https://www.kernel.org/")) "Linux,")
     (a ((href "https://www.gentoo.org/")) "Gentoo,")
     (a ((href "https://www.gnu.org/software/emacs/")) "Emacs,")
     (a ((href "https://www.python.org/")) "Python,")
     (a ((href "https://racket-lang.org/")) "Racket") "and any other"
     (a ((href "https://lisp-lang.org/")) "Lisp") "or"
     (a ((href "http://www.schemers.org/")) "Scheme")
     "dialects/implementations.")
    (p "Regarding social media - find me on"
       (a ((href "https://functional.cafe/@xgqt")
           (rel "me"))
          "Mastodon")
       "in Functional.Cafe.")))

(define xexpr-index
  (xexpr-main "XGQT's site" xexpr-index-top xexpr-index-right))

(define xexpr-donate-top
  '(div ((id "top")) (h1 "Donations")))

(define xexpr-donate-right
  '(div
    ((id "right"))
    (h2 "Thanks for all the support!" (i ((class "fa fa-heart") (title "love"))))
    (p "If you want to support my open source work consider donating.")
    (ul
     (li
      (a
       ((href "https://www.paypal.com/paypalme/xgqt"))
       (i ((class "fa fa-paypal")))
       "PayPal")))))

(define xexpr-donate
  (xexpr-main "XGQT's site: Donations" xexpr-donate-top xexpr-donate-right))

(define xexpr-gpg-top
  '(div ((id "top")) (h1 "GPG Key")))

(define xexpr-gpg-right
  '(div
    ((id "right"))
    (p "Fingerprint:")
    (a
     ((href "https://keys.openpgp.org/search?q=0x14D74A1F43A6AC3C"))
     "9B0A 4C5D 02A3 B43C 9D6F"
     "D6B1 14D7 4A1F 43A6 AC3C")
    (div
     ((class "code-block"))
     (p "gpg --keyserver keys.openpgp.org --recv-keys 9B0A4C5D02A3B43C9D6FD6B114D74A1F43A6AC3C")
     (p "gpg --list-keys 9B0A4C5D02A3B43C9D6FD6B114D74A1F43A6AC3C"))
    (div
     (p
      "Also available on GitLab:"
      (a ((href "https://gitlab.com/xgqt.gpg")) "gitlab.com/xgqt.gpg")))))

(define xexpr-gpg
  (xexpr-main "XGQT's site: GPG Key" xexpr-gpg-top xexpr-gpg-right))

(define xexpr-projects-top
  '(div ((id "top")) (h1 "Notable Projects")))

(define xexpr-projects-right
  '(div
    ((id "right"))
    (ul
     (li
      "Racket"
      (ul
       (li
        (a ((href "https://gentoo-racket.gitlab.io/")) "Gentoo-Racket")
        (br)
        "Group of libraries and tools to support"
        (a ((href "https://racket-lang.org/")) "Racket")
        "programming language ecosystem on"
        (a ((href "https://www.gentoo.org/")) "Gentoo")
        "systems.")
       (li
        (a ((href "https://gitlab.com/xgqt/racket-req/")) "Req")
        (br)
        "Dependency manager for Racket projects.")
       (li
        (a ((href "https://gitlab.com/xgqt/racket-rawk/")) "Rawk")
        (br)
        "AWK-like scripting in Racket.")))
     (li
      "Python"
      (ul
       (li
        (a ((href "https://gitlab.com/xgqt/xgqt-python-app-qbwrap/")) "QBwrap")
        (br)
        "Quick Bubblewrap chroot management tool.")
       (li
        (a ((href "https://gitlab.com/xgqt/python-logrot/")) "LogRot")
        (br)
        "Log rotation tool.")
       (li
        (a ((href "https://gitlab.com/xgqt/python-etask/")) "ETask")
        (br)
        "CLI interface to GNU Emacs.")))
     (li
      "C#"
      (ul
       (li
        (a ((href "https://gitlab.com/xgqt/xgqt-csharp-app-fedimpost/")) "Fedimpost")
        (br)
        "Importer of Mastodon posts to blogging frameworks.")))
     (li
      "Java"
      (ul
       (li
        (a ((href "https://github.com/br1gh/Kronos/")) "Kronos")
        (br)
        "JAVA Swing application inspired by UNIX"
        (a ((href "https://en.wikipedia.org/wiki/cron")) "Cron")
        "tool.")))
     (li
      "GNU Emacs"
      (ul
       (li
        (a ((href="https://gitlab.com/xgqt/emacs-websearch/")) "WebSearch")
        (br)
        "Query search engines from Emacs.")
       (li
        (a ((href="https://gitlab.com/xgqt/emacs-el-fetch/")) "El-Fetch")
        (br)
        "Show system information in"
        (a ((href="https://github.com/dylanaraps/neofetch/")) "Neofetch-like")
        "style (eg CPU, RAM)."))))))

(define xexpr-projects
  (xexpr-main "XGQT's site: Notable Projects" xexpr-projects-top xexpr-projects-right))

(define html-content-assoc
  (hash "index.html"    xexpr-index
        "donate.html"   xexpr-donate
        "gpg.html"      xexpr-gpg
        "projects.html" xexpr-projects))
